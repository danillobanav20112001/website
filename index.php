<?php include 'header.php'; ?>

<div class="content">
	<?php include 'vendor/generateDataMain.php'; ?>
</div>
<div class="expanded-overlay"></div>
<div class="expanded-loadImage">
	<div class="avatar-container" ondrop="dropHandler(event);" ondragover="dragOverHandler(event);">
		<label>
			<img style="height: 50vh;object-fit: cover; max-width: 100%;border: 2px dashed #ccc;" class="avatar-image" id="drop-zone" alt="Avatar" src="img/non.jpg" alt="Card image cap">
			<span class="avatar-text">Выберите картинку</span>
            <form action="vendor/loadImage.php" method="post" enctype="multipart/form-data">
				<input type="file" id="image_content" name="image_content" onchange="handleFileSelect(event);" style="display: none; "value="img/non.jpg">
				<input class="loadImageBtn" type="submit" value="Загрузить">
			</form>
			
		</label>
	</div> 
<div class="close"></div>
</div>
<script>
function dropHandler(event) {
    event.preventDefault();

    if (event.dataTransfer.items) {
        // Используем DataTransferItemList интерфейс для доступа к файлам
        for (var i = 0; i < event.dataTransfer.items.length; i++) {
            // Если пользователь выбрал файл
            if (event.dataTransfer.items[i].kind === 'file') {
                var file = event.dataTransfer.items[i].getAsFile();
                // Производим дальнейшие действия с файлом, например, загрузка или отображение
                handleFile(file);
            }
        }
    } else {
        // Используем DataTransfer интерфейс для доступа к файлам
        for (var i = 0; i < event.dataTransfer.files.length; i++) {
            // Если пользователь выбрал файл
            if (event.dataTransfer.files[i].kind === 'file') {
                var file = event.dataTransfer.files[i];
                // Производим дальнейшие действия с файлом, например, загрузка или отображение
                handleFile(file);
            }
        }
    }
}

function handleFile(file) {
    var reader = new FileReader();
    reader.onload = function(event) {
        // Заменяем фоновое изображение на загруженное
        document.getElementById('drop-zone').src = event.target.result;
    };
    reader.readAsDataURL(file);
}

function handleFileSelect(event) {
    var file = event.target.files[0];
    handleFile(file);
}
	function dragOverHandler(event) {
		event.preventDefault();
		event.dataTransfer.dropEffect = 'copy';
	}

    document.querySelectorAll('.square').forEach(item => {
    item.addEventListener('click', () => {
        item.classList.add('expanded');
		document.querySelector('.expanded-overlay').classList.add('expanded');
		const body = document.querySelector('body');
        if (item.classList.contains('expanded')) {
            body.style.overflow = 'hidden';
        } else {
            body.style.overflow = '';
        }
    });
});
	
	document.querySelectorAll('.expanded-overlay').forEach(item => {
        item.addEventListener('click', () => {
            item.classList.toggle('expanded');
			document.querySelector('.expanded-loadImage').classList.remove('expanded');
			document.querySelector('.square.expanded').classList.remove('expanded');
			
			const body = document.querySelector('body');
			if (item.classList.contains('expanded')) {
				body.style.overflow = 'hidden';
			} else {
				body.style.overflow = '';
			}
        });
    });

    document.querySelectorAll('.close').forEach(closeBtn => {
        closeBtn.addEventListener('click', e => {
            e.stopPropagation();
            const square = closeBtn.parentElement;
            square.classList.remove('expanded');
			document.querySelector('.expanded-overlay').classList.remove('expanded');
			document.querySelector('.expanded-loadImage').classList.remove('expanded');
			const body = document.querySelector('body');
				body.style.overflow = '';
        });
    });
	document.querySelectorAll('.loadImg').forEach(item => {
        item.addEventListener('click', () => {
            //item.classList.toggle('expanded');
			document.querySelector('.expanded-overlay').classList.add('expanded');
			document.querySelector('.expanded-loadImage').classList.add('expanded');
        });
    });
	// Находим элемент с классом "saveImg"
    const saveImgButton = document.querySelector('.saveImg');

    // Добавляем обработчик события клика
	document.querySelectorAll('.saveImg').forEach(item => {
        item.addEventListener('click', () => {
		// Получаем ссылку на изображение
		const imagePath = document.querySelector('.square.expanded img').src;

		// Извлекаем имя файла и расширение
		const fileName = imagePath.substring(imagePath.lastIndexOf('/') + 1);
		const fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);

		// Создаем ссылку для скачивания
		const downloadLink = document.createElement('a');
		downloadLink.href = imagePath;
		downloadLink.download = fileName; // Используем извлеченное имя файла
		downloadLink.click();
		});
	});
</script>

<?php include 'footer.php'; ?>		
